# wayland

Display compositor infrastructure. https://wayland.freedesktop.org

# Official documentation
* [The Wayland Protocol](https://wayland.freedesktop.org/docs/html/index.html)
  2012 Kristian Høgsberg, Intel Corporation
  * Chapter 4. Wayland Protocol and Model of Operation
* [Programming Wayland Clients](https://jan.newmarch.name/Wayland/index.html)
  2014 Jan Newmarch
  * 6.3 Connecting to a server

# Wikipedia
* [*EGL (API)*](https://en.m.wikipedia.org/wiki/EGL_(API))
* [*Wayland (display server protocol)*
  ](https://en.m.wikipedia.org/wiki/Wayland_(display_server_protocol))
* [List of display servers
  ](https://en.m.wikipedia.org/wiki/List_of_display_servers#Wayland)
  Section Wayland

# Languages bindings
* Rust
  * https://crates.io/search?q=wayland
  * Rust implementation of the wayland protocol (client and server).
    * [Smithay/wayland-rs](https://github.com/Smithay/wayland-rs)
  * [winit](https://crates.io/crates/winit) - Cross-platform window creation and management in Rust
* WL - Perl Wayland protocol binding
  * https://metacpan.org/pod/release/LKUNDRAK/WL-0.9/lib/WL.pm

# Debian packages
* https://tracker.debian.org/pkg/wayland
* https://tracker.debian.org/pkg/waylandpp
* https://tracker.debian.org/pkg/wayland-protocols
